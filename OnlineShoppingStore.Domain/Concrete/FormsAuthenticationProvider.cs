﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineShoppingStore.Domain.Abstract;

namespace OnlineShoppingStore.Domain.Concrete
{
    public class FormsAuthenticationProvider : IAuthentication
    {
        private readonly EFDbContext context = new EFDbContext();
        public bool Authenicate(string username, string password)
        {
            //FirstOrDefault return the first row other return NULL
            //NOTES:  to use the database user table, need to go to NuGet Package Manager Console
            // THEN typein: enable-migrations, as this is code first approach, in the dBcontext Domain
            var result = context.Users.FirstOrDefault(u => u.UserId == username && 
                                                           u.Password == password);
            if (result == null)
                return false;

            //otherwise return true
            return true;
        }


        public bool Logout()
        {
            return true;
        }
    }
}
