﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingStore.Domain.Abstract;
using OnlineShoppingStore.WebUI.Models;

namespace OnlineShoppingStore.WebUI.Controllers
{
#pragma warning disable CS0246 // The type or namespace name 'Controller' could not be found (are you missing a using directive or an assembly reference?)
    public class ProductController : Controller
#pragma warning restore CS0246 // The type or namespace name 'Controller' could not be found (are you missing a using directive or an assembly reference?)
    {
        //Use contractor injection
        private readonly IProductRepository repository;
        public int PageSize = 4;
        public ProductController(IProductRepository repo)
        {
            repository = repo;
        }
#pragma warning disable CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        public ViewResult List(string category, int page = 1)
#pragma warning restore CS0246 // The type or namespace name 'ViewResult' could not be found (are you missing a using directive or an assembly reference?)
        {
            ProductsListViewModel model = new ProductsListViewModel
            {
                Products = repository.Products
                    .Where(p => category == null || p.Category == category)
                    .OrderBy(p => p.ProductId)
                    .Skip((page -1) * PageSize)
                    .Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                        // if the first condition is true, execute the first repository.prodcuts.count()
                    TotalItems = category == null? 
                                 repository.Products.Count() : 
                        // if the condition is fall then execute the bellow 
                        repository.Products.Where(p => p.Category == category).Count()
                },
                CurrentCategory = category
            };
            return View(model);
     
        }
    }
}